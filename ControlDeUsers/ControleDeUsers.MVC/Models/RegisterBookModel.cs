﻿using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControleDeUsers.MVC.Models
{
    public class RegisterBookModel
    {
        public int Code { get; set; }
        public string Title { get;  set; }
        public int Year { get;  set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public string Genero { get; set; }
    }

}