﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ControleDeUsers.MVC.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}