﻿using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControleDeUsers.MVC.Controllers.Account
{
    public class LoginController : Controller
    {
        // GET: Login
        private IUserService _service;
        public LoginController(IUserService service)
        {
            _service = service;
        }
        public LoginController()
        {

        }
        [HttpGet]
        public ActionResult login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            try
            {
                _service.Authenticate(model.Email, model.Password);
                return RedirectToAction("Index", "Home");
            }
            catch(Exception e )
            {
                ModelState.AddModelError("DefaultErrorMessage", e.Message);
                return View(model);
            }
        }
        protected override void Dispose(bool disposing)
        {
            _service.Dispose();
        }
    }
}