﻿using ControleDeUsers.MVC.Models;
using ControleDeUsers.Domain.Contracts.Services;
using System;
using System.Web.Mvc;

namespace ControleDeUsers.MVC.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        private IUserService _service;
        public RegisterController(IUserService service)
        {
            _service = service;
        }
        public RegisterController()
        {

        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
       [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                _service.Register(model.Name, model.Email, model.Password, model.ConfirmPassword);
                return RedirectToAction("Login","Login");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("DefaultErrorMessage", e.Message);
                return View(model);
            }
        }
        protected override void Dispose(bool disposing)
        {
            _service.Dispose();
        }
    }
}