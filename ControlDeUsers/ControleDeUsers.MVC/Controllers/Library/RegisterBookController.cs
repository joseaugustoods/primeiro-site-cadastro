﻿using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ControleDeUsers.MVC.Controllers.Library
{
    public class RegisterBookController : Controller
    {
        private List<string> authors;
        private IAuthorService _service;
        private IBookService _service2;

        public RegisterBookController(IAuthorService service, IBookService service2)
        {
            _service = service;
            authors = _service.ListAll();
            _service2 = service2;
        }
        public RegisterBookController()
        {

        }
        // GET: RegisterBook
        public ActionResult Book()
        {
            SelectList list = new SelectList(authors,"Name");
            ViewBag.ListAuthor = list;
            return View();
        }
        [HttpPost]
        public ActionResult Book(RegisterBookModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            _service2.Register(model.Code, model.Title, model.Year, model.Author, model.Publisher, model.Genero);
            return RedirectToAction("Index", "Home");

        }
        protected override void Dispose(bool disposing)
        {
            _service.Dispose();
        }
    }
}