﻿using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ControleDeUsers.Business.Services
{
    public class AuthorService : IAuthorService
    {
        private IAuthorRepository _repository;
        //private object dataTabble;


        public AuthorService(IAuthorRepository repository)
        {
            this._repository = repository;
        }
        public void Dispose()
        {
            _repository.Dispose();
        }

        public List<string> ListAll()
        {
            List<string> list = new List<string>();
            string connectionString = "Server=LAPTOP-C8DIC8AI\\sa;Initial Catalog=controllibrary;User ID=sa;Password=Pegazus@@;Pooling=False";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                /*DataTable databases = con.GetSchema("Databases\\spausercontrol\\Tables");
                foreach (DataRow database in databases.Rows)
                {
                    String databaseName = database.Field<String>("Name");
                }*/
                SqlCommand cmd = new SqlCommand("SELECT * FROM Author ORDER BY  Name", con);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    list.Add(dr.GetString(0));
                }
                return list;
            }
        }
    }
}
