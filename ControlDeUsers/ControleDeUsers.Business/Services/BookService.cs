﻿using ControleDeUsers.Common.Resourses;
using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Business.Services
{
    public class BookService : IBookService
    {

        private IbookRepository _repository;
        //private object dataTabble;

        public DataTable DependencyResolver { get; private set; }

        public BookService(IbookRepository repository)
        {
            this._repository = repository;
        }
        public void Dispose()
        {
            _repository.Dispose();
        }


        public Book GetByTitle(string title)
        {
            var book = _repository.Get(title);
            if (book == null)
            {
                throw new Exception(Errors.BookNotFound);
            }
            return book;
        }

        public void Register(int code, string title, int year, string author, string publisher, string genero)
        {
            var book = new Book();
            book.setCode(code);
            book.setTitle(title);
            book.setYear(year);
            book.setAuthor(author);
            book.setPublisher(publisher);
            book.setGenero(genero);
            _repository.Create(book);

        }
    }
}
