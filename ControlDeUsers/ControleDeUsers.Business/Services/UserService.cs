﻿using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.Domain.Domain;
using System;
using System.Data;
using ControleDeUsers.Common.Validation;
using ControleDeUsers.Common.Resourses;

namespace ControleDeUsers.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        //private object dataTabble;

        public DataTable DependencyResolver { get; private set; }

        public UserService(IUserRepository repository)
        {
            this._repository = repository;
        }

        public User Authenticate(string email, string password)
        {
            var user = GetByEmail(email);
            if (user.Password != PasswordAssertionConcern.Encrypt(password))
                throw new Exception(Errors.InvalidCredentials);

            return user;
        }

        public void ChangeInformation(string email, string name)
        {
            var user = GetByEmail(email);
            user.ChangeName(name);
            user.Validade();
            _repository.Update(user);
        }

        public void ChangePassword(string email, string password, string newPassword, string confirmNewPassword)
        {
            var user = Authenticate(email, password);
            user.SetPassword(newPassword, confirmNewPassword);
            user.Validade();
            _repository.Update(user);
        }

        public User GetByEmail(string email)
        {
            var user = _repository.Get(email);
            if(user == null)
            {
                throw new Exception(Errors.UserNotFound);            }
            return user;
        }

        public void Register(string nome, string email, string password, string confirmPassword)
        {
            var hasUser = _repository.Get(email);
            if(hasUser != null)
            {
                throw new Exception(Errors.DuplicateEmail);
            }
            var user = new User(nome, email);
            user.SetPassword(password, confirmPassword);
            user.Validade();
            _repository.Create(user);
        }

        public string ResetPassword(string email)
        {
            var user = GetByEmail(email);
            var password = user.ResetPassword();
            user.Validade();

            _repository.Update(user);
            return password;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        public object GetByRange(int v1, int v2)
        {
            throw new NotImplementedException();
        }
    }
}
