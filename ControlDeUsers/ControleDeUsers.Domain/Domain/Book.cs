﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Domain.Domain
{
    public class  Book
    {
        [Key]
        public int Code { get; private set; }
        public string Title { get; private set; }
        public int Year { get; private set; }
        public string Author { get; private set; }
        public string Publisher { get; private set; }
        public string Genero { get; private set; }

        public Book()
        {

        }
        public void setCode(int code)
        {
            Code = code;
        }
        public void setTitle(string title)
        {
            Title = title;
        }
        public void setYear(int year)
        {
            Year = year;
        }
        public void setAuthor(string author)
        {
            Author = author;
        }
        public void setPublisher(string publisher)
        {
            Publisher = publisher;
        }
        public void setGenero(string genero)
        {
            Genero = genero;
        }
    }
}
