﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Domain.Domain
{
    public class Author
    {
        [Key]
        public string Name { get; private set; }
        public Author(string name)
        {
            this.Name = name;
        }
        protected Author()
        {

        }
    }
}
