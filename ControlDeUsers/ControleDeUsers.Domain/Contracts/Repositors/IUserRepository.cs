﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ControleDeUsers.Domain.Contracts.Repositors
{
    public interface IUserRepository : IDisposable //acessa dados e fecha
    {
        User Get(string email);
        User Get(Guid user);
        void Create(User user);
        void Update(User user);
        void Delete(User user);
    }
}
