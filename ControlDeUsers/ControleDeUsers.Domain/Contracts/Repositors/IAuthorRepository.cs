﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Domain.Contracts.Repositors
{
    public interface IAuthorRepository : IDisposable
    {
        Author Get(string name);
        void Create(Author author);
        void Update(Author author);
        void Delete(Author author);
    }
}
