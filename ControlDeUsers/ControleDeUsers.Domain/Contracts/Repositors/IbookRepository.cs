﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Domain.Contracts.Repositors
{
    public interface IbookRepository : IDisposable
    {
        Book Get(string title);
        void Create(Book book);
        void Delete(Book book);
        void Update(Book book);
    }
}
