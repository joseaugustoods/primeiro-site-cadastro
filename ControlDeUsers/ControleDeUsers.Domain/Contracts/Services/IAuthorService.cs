﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Domain.Contracts.Services
{
    public interface IAuthorService : IDisposable
    {
        List <string> ListAll();
    }
}
