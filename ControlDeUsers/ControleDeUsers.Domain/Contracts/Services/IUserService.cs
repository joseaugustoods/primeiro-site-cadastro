﻿using ControleDeUsers.Domain.Domain;
using System;

namespace ControleDeUsers.Domain.Contracts.Services
{
    public interface IUserService : IDisposable //preparar o dominio em uma interface
    {
        User Authenticate(string email, string password);
        User GetByEmail(string email);
        void Register(string nome, string email, string password, string confirmPassword);
        void ChangeInformation(string email, string name);
        void ChangePassword(string email, string password, string newPassword, string confirmNewPassword);

        string ResetPassword(string email);

        object GetByRange(int v1, int v2);

    }
}
