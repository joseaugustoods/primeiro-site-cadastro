﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Domain.Contracts.Services
{
    public interface IBookService : IDisposable
    {
        Book GetByTitle(string title);

        void Register(int code, string title, int year, string author, string publisher, string genero);
    }
}
