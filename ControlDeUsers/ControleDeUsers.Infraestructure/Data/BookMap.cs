﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Infraestructure.Data
{
    class BookMap : EntityTypeConfiguration<Book>
    {
        public BookMap()
        {
            ToTable("Book").
                Property(x => x.Code).IsRequired();
            Property(x => x.Title).HasMaxLength(60).IsRequired();
            Property(x => x.Year).IsRequired();
            Property(x => x.Author).HasMaxLength(60);
            Property(x => x.Publisher).HasMaxLength(60).IsRequired();
            Property(x => x.Genero).HasMaxLength(60).IsRequired();
        }
    }
}
