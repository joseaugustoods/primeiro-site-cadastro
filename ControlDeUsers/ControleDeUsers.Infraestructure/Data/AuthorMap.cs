﻿using ControleDeUsers.Domain.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace ControleDeUsers.Infraestructure.Data
{
    public class AuthorMap : EntityTypeConfiguration<Author>
    {
        public AuthorMap()
        {
            ToTable("Author").
            Property(x => x.Name).HasMaxLength(60).IsRequired();
        }
    }
}
