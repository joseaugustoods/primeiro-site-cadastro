﻿using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Domain;
using ControleDeUsers.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Infraestructure.Repositories
{
    public class UserRepository : IUserRepository// muito interessante o IU principalmente para testes
    {
        private AppDataContext _context;//para usar meus usuarios aqui

        public UserRepository(AppDataContext context)
        {
            this._context = context;
        }

        public void Create(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();//para salvar no banco
        }

        public void Delete(User user)
        {
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
        public void Update(User user)
        {
            _context.Entry<User>(user).State = System.Data.Entity.EntityState.Modified;//para obj modificado
            _context.SaveChanges();
        }
        public User Get(string email)
        {
            return _context.Users.Where(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();
        }

        public User Get(Guid id)
        {
            return _context.Users.Where(x => x.Id == id).FirstOrDefault();
        }


        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
