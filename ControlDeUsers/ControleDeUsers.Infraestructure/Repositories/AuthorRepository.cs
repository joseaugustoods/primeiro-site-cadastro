﻿using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Domain;
using ControleDeUsers.Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Infraestructure.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private AppDataContext _context  = new AppDataContext();//para usar meus usuarios aqui

        /*public AuthorRepository(AppDataContext context)
        {
            this._context = context;
        }*/

        public void Create(Author author)
        {
            _context.Authors.Add(author);
            _context.SaveChanges();
        }

        public void Delete(Author author)
        {
            _context.Authors.Remove(author);
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public Author Get(string name)
        {
            return _context.Authors.Where(x => x.Name.ToLower() == name.ToLower()).FirstOrDefault();
        }

        public void Update(Author author)
        {
            _context.Entry<Author>(author).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
