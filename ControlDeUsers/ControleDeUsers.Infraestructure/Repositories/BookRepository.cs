﻿using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Domain;
using ControleDeUsers.Infraestructure.Data;
using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeUsers.Infraestructure.Repositories
{
    public class BookRepository : IbookRepository
    {
        private AppDataContext _context;
        public BookRepository(AppDataContext context)
        {
            _context = context;
        }
        public void Create(Book book)
        {
           
            _context.Books.Add(book);
            try
            {
                 _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                .SelectMany(x => x.ValidationErrors)
                .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public void Delete(Book book)
        {
            _context.Books.Remove(book);
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public Book Get(string title)
        {
            return _context.Books.Where(x => x.Title.ToLower() == title.ToLower()).FirstOrDefault();
        }

        public void Update(Book book)
        {
            _context.Entry<Book>(book).State = System.Data.Entity.EntityState.Modified;//para obj modificado
            _context.SaveChanges();
        }
    }
}
