﻿using ControleDeUsers.Business.Services;
using ControleDeUsers.Domain.Contracts.Repositors;
using ControleDeUsers.Domain.Contracts.Services;
using ControleDeUsers.Domain.Domain;
using ControleDeUsers.Infraestructure.Data;
using ControleDeUsers.Infraestructure.Repositories;
using ControleDeUsers.Services;
using Unity;
using Unity.Lifetime;

namespace ControleDeUsers.Startup
{
    public static class DependencyResolver//o projeto de API connhece esse. Basicamente cria uma instancia concreta de cada container, alimentando todas as suas dependencias
    {//como se fosse um construtor padrao
        public static void Resolve(UnityContainer container)
        {
            //para resolver as dependencias com o unity

            container.RegisterType<AppDataContext, AppDataContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserService, UserService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthorRepository, AuthorRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IbookRepository, BookRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IBookService, BookService>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthorService, AuthorService>(new HierarchicalLifetimeManager());

            //container.RegisterType<User, User>(new HierarchicalLifetimeManager());
        }
    }
}
