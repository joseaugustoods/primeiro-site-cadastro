﻿using ControleDeUser.Domain.Contracts.Repositors;
using ControleDeUser.Domain.Domain;
using ControleDeUser.Infraestructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new User("usuario2", "usuario2@hotmail.com");
            user.SetPassword("1234567", "1234567");
            user.Validade();
            using (IUserRepository userRep = new UserRepository())
            {
                userRep.Create(user);

            }
            using (IUserRepository userRep = new UserRepository())
            {
                var usr = userRep.Get("usuario2@hotmail.com");
                Console.WriteLine(usr.Email);

            }
            Console.ReadKey();
        }
    }
}
