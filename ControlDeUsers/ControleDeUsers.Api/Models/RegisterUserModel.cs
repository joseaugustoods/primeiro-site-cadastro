﻿using ControleDeUsers.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ControleDeUsers.Api.Models
{
    public class RegisterUserModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public RegisterUserModel()
        {

        }
        public RegisterUserModel(User user)
        {
            Name = user.Name;
            Email = user.Email;
            Password = user.Password;
        }

    }
}